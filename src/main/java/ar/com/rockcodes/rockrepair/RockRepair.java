package ar.com.rockcodes.rockrepair;


import java.util.logging.Level;

import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import ar.com.rockcodes.rockrepair.commands.Comandos;
import ar.com.rockcodes.rockrepair.util.VaultHelper;
import net.md_5.bungee.api.ChatColor;


public class RockRepair extends JavaPlugin {

	public static RockRepair plugin;


	@Override
	public void onDisable() {

	}

	@Override
	public void onEnable() {
		
		RockRepair.plugin= this;
		saveDefaultConfig();
		Settings.loadSettings();
		getCommand("reparar").setExecutor(new Comandos());
		
		if (!VaultHelper.setupEconomy()) {
		    getLogger().warning("Could not set up economy! - Running without an economy.");
		}
		if (!VaultHelper.setupPermissions()) {
		    getLogger().severe("Cannot link with Vault for permissions! Disabling plugin!");
		    getServer().getPluginManager().disablePlugin(this);
		    return;
		}
		
	}
	
	public static void debug(String str){
		
		if(Settings.debug){
			ConsoleCommandSender console = RockRepair.plugin.getServer().getConsoleSender();
		    console.sendMessage(ChatColor.RED+"[DEBUG]"+ChatColor.AQUA+str);
		}
	}
	
	
	public static void log(String str,Level level){
		
		RockRepair.plugin.getServer().getLogger().log(level, str);
		
	}
	
	

}
