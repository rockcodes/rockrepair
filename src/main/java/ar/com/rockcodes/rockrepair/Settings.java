package ar.com.rockcodes.rockrepair;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;




public class Settings {

	
	public static Boolean debug;
		
	public static double material_wood = 20;
	public static double material_stone = 20;
	public static double material_leather = 20;
	public static double material_iron = 20;
	public static double material_gold = 20;
	public static double material_diamond = 20;
	public static double material_chain = 20;
	public static double extra_bow = 20;
	public static double extra_fishrod = 20;	
	public static double vipdiscount = 10;
	public static Map<Enchantment,Double>  enchants = new HashMap<Enchantment,Double>();
	
	public static void loadSettings(){
		enchants.clear();
		FileConfiguration config = RockRepair.plugin.getConfig() ;
		
		
		//Material CONFIG
		Settings.vipdiscount = config.getDouble("vipdiscount");
		Settings.material_wood = config.getDouble("material.wood");
		Settings.material_stone = config.getDouble("material.stone");
		Settings.material_leather = config.getDouble("material.leather");
		Settings.material_iron = config.getDouble("material.iron");
		Settings.material_gold = config.getDouble("material.gold");
		Settings.material_diamond = config.getDouble("material.diamond");
		Settings.material_chain = config.getDouble("material.chain");
		
		Settings.extra_bow = config.getDouble("extra.bow");
		Settings.extra_fishrod = config.getDouble("extra.fish_rod");
		
		Settings.debug = config.getBoolean("general.debug",true);
		
		if(config.contains("enchants")){
			for(String enchant : config.getConfigurationSection("enchants").getKeys(false)){
				Settings.enchants.put(Enchantment.getByName(enchant), config.getDouble("enchants."+enchant+".price",100));
			}
		}else{	
			RockRepair.log("Error: No existe la seccion enchants en config.yml", Level.SEVERE);
		}

		
	}
	
	
}
