package ar.com.rockcodes.rockrepair.commands;


import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Repairable;

import ar.com.rockcodes.rockrepair.RockRepair;
import ar.com.rockcodes.rockrepair.Settings;
import ar.com.rockcodes.rockrepair.util.Msg;
import ar.com.rockcodes.rockrepair.util.VaultHelper;
import net.milkbowl.vault.economy.EconomyResponse;

public class Comandos implements CommandExecutor {

	
	public boolean onCommand(CommandSender sender, Command command,String label, String[] args) {
		
		if (sender instanceof Player) {
			Player player = (Player)sender;
			if(args.length==0){repair_main(player,args); return true;}
			if(player.isOp()){
				
				switch(args[0]){
				case "reload": repair_reload(player); return true;	
				}
				
			}
			switch(args[0]){
				case "help": repair_help(player); return true;
				case "costo": repair_precio(player); return true;
				case "precio": repair_precio(player); return true;
				case "version": repair_version(player); return true;	
				default: repair_main(player,args); return true;	
			}
			
			
			
			
        } else {
        	if(args.length!=0){
				switch(args[0]){
				case "reload": repair_reload(sender); return true;	
				}
        	}
        	
           sender.sendMessage("Usted debe ser un jugador!");
           return false;
        }
       

	}

	private void repair_precio(Player player) {
		ItemStack item = player.getItemInHand();
		if(isreparable(item)){
			Material material = item.getType();
			double precio = calcular_precio(item,material);
			if(precio == -1) {
				Msg.sendMsg(player, "No tienes permisos para reparar este item, utiliza el yunque.");
				return ; //NO ESTA EN LA LISTA DE REPAIR
			}
			if(VaultHelper.permission.has(player, "rockrepair.vipdiscount")){
				precio = precio - (precio*(Settings.vipdiscount/100));
				Msg.sendMsg(player, "Se te ha aplicado un descuento VIP de: "+Settings.vipdiscount+"%");
			}
			Msg.sendMsg(player, "Costo de reparaci�n: "+precio+"$");
			
		}else{
			Msg.sendMsg(player, "Este item no es reparable.");
		}
		
	}

	private void repair_help(Player player) {
		Msg.sendMsg(player, "/reparar - repara el item en tu mano");
		Msg.sendMsg(player, "/reparar precio - consulta el precio que costar�a reparar el item en tu mano");
	}

	private void repair_version(CommandSender player) {
		Msg.sendMsg(player, "Version:"+RockRepair.plugin.getDescription().getVersion());
		
	}

	private void repair_reload(CommandSender player) {
		Settings.loadSettings();
	}

	
	
	private boolean isreparable(ItemStack item){
		Material material = item.getType();
		if (item == null || item.getType().isBlock() || item.getDurability() == 0 || material.isBlock() || material.getMaxDurability() < 1){
			return false;
		}
		return true;
	}
	
	private void repair_main(Player player, String[] args) {
		ItemStack item = player.getItemInHand();
		if(isreparable(item)){
			Material material = item.getType();

			double precio = calcular_precio(item,material);
			if(precio == -1) {
				Msg.sendMsg(player, "No tienes permisos para reparar este item, utiliza el yunque.");
				return ; //NO ESTA EN LA LISTA DE REPAIR
			}
			
			if(VaultHelper.permission.has(player, "rockrepair.vipdiscount")){
				precio = precio - (precio*(Settings.vipdiscount/100));
				Msg.sendMsg(player, "Se te ha aplicado un descuento VIP de: "+Settings.vipdiscount+"%");
			}
			
			EconomyResponse operacion = VaultHelper.econ.withdrawPlayer(player.getName(), precio);
			if(operacion.transactionSuccess()){
				item.setDurability((short) 0);
				Msg.sendMsg(player, "Su item ha sido reparado.");
			}else{
				Msg.sendMsg(player, "No tienes suficiente dinero para reparar este item.");
			}
			Msg.sendMsg(player, "Costo de reparaci�n: "+precio+"$");

			
			
			
		}else{
			Msg.sendMsg(player, "Este item no es reparable.");
		}

		
	}
	
	
	private double calcular_precio(ItemStack item,Material material){

		double precio = 0;
		double preciobase = get_preciobase(material);
		if(preciobase == -1) return -1; //NO ESTA EN LA LISTA DE REPAIR
		
		precio += preciobase;
		
		
		double precioenchant = 0;
		Map<Enchantment, Integer> enchants = item.getEnchantments();
		
		for(Entry<Enchantment, Integer> ench :enchants.entrySet()){
			if(Settings.enchants.containsKey(ench.getKey())){
				precioenchant += Settings.enchants.get(ench.getKey())*ench.getValue();
			}
		}
		
		precio += precioenchant;

		return precio;
	}
	private double get_preciobase(Material material){
		double precio= 0;
		//ARMADURA
		if(material.equals(Material.IRON_CHESTPLATE))precio = precio_armadura("iron");
		else if(material.equals(Material.GOLD_CHESTPLATE))precio = precio_armadura("gold");
		else if(material.equals(Material.LEATHER_CHESTPLATE))precio = precio_armadura("leather");
		else if(material.equals(Material.DIAMOND_CHESTPLATE))precio = precio_armadura("diamond");
		else if(material.equals(Material.CHAINMAIL_CHESTPLATE))precio = precio_armadura("chain");
		//CASCO
		else if(material.equals(Material.IRON_HELMET))precio = precio_casco("iron");
		else if(material.equals(Material.GOLD_HELMET))precio = precio_casco("gold");
		else if(material.equals(Material.LEATHER_HELMET))precio = precio_casco("leather");
		else if(material.equals(Material.DIAMOND_HELMET))precio = precio_casco("diamond");
		else if(material.equals(Material.CHAINMAIL_HELMET))precio = precio_casco("chain");
		//PANTALON
		else if(material.equals(Material.IRON_LEGGINGS))precio = precio_pantalon("iron");
		else if(material.equals(Material.GOLD_LEGGINGS))precio = precio_pantalon("gold");
		else if(material.equals(Material.LEATHER_LEGGINGS))precio = precio_pantalon("leather");
		else if(material.equals(Material.DIAMOND_LEGGINGS))precio = precio_pantalon("diamond");
		else if(material.equals(Material.CHAINMAIL_LEGGINGS))precio = precio_pantalon("chain");
		//BOTAS
		else if(material.equals(Material.IRON_BOOTS))precio = precio_botas("iron");
		else if(material.equals(Material.GOLD_BOOTS))precio = precio_botas("gold");
		else if(material.equals(Material.LEATHER_BOOTS))precio = precio_botas("leather");
		else if(material.equals(Material.DIAMOND_BOOTS))precio = precio_botas("diamond");
		else if(material.equals(Material.CHAINMAIL_BOOTS))precio = precio_botas("chain");
		
		
		//PICO
		else if(material.equals(Material.IRON_PICKAXE))precio = precio_pico("iron");
		else if(material.equals(Material.GOLD_PICKAXE))precio = precio_pico("gold");
		else if(material.equals(Material.WOOD_PICKAXE))precio = precio_pico("wood");
		else if(material.equals(Material.DIAMOND_PICKAXE))precio = precio_pico("diamond");
		else if(material.equals(Material.STONE_PICKAXE))precio = precio_pico("stone");
		//PALA
		else if(material.equals(Material.IRON_SPADE))precio = precio_pala("iron");
		else if(material.equals(Material.GOLD_SPADE))precio = precio_pala("gold");
		else if(material.equals(Material.WOOD_SPADE))precio = precio_pala("wood");
		else if(material.equals(Material.DIAMOND_SPADE))precio = precio_pala("diamond");
		else if(material.equals(Material.STONE_SPADE))precio = precio_pala("stone");		
		//HACHA
		else if(material.equals(Material.IRON_AXE))precio = precio_hacha("iron");
		else if(material.equals(Material.GOLD_AXE))precio = precio_hacha("gold");
		else if(material.equals(Material.WOOD_AXE))precio = precio_hacha("wood");
		else if(material.equals(Material.DIAMOND_AXE))precio = precio_hacha("diamond");
		else if(material.equals(Material.STONE_AXE))precio = precio_hacha("stone");	
		//HOZ
		else if(material.equals(Material.IRON_HOE))precio = precio_hoz("iron");
		else if(material.equals(Material.GOLD_HOE))precio = precio_hoz("gold");
		else if(material.equals(Material.WOOD_HOE))precio = precio_hoz("wood");
		else if(material.equals(Material.DIAMOND_HOE))precio = precio_hoz("diamond");
		else if(material.equals(Material.STONE_HOE))precio = precio_hoz("stone");	
		//ESPADA
		else if(material.equals(Material.IRON_SWORD))precio = precio_espada("iron");
		else if(material.equals(Material.GOLD_SWORD))precio = precio_espada("gold");
		else if(material.equals(Material.WOOD_SWORD))precio = precio_espada("wood");
		else if(material.equals(Material.DIAMOND_SWORD))precio = precio_espada("diamond");
		else if(material.equals(Material.STONE_SWORD))precio = precio_espada("stone");
		//ARCO
		else if(material.equals(Material.BOW))precio = precio_arco();
		//CA�A
		else if(material.equals(Material.FISHING_ROD))precio = precio_ca�a();
		else return -1;
		return precio;
	}
	private double precio_ca�a(){
		return Settings.extra_fishrod;
	}
	private double precio_arco(){
		return Settings.extra_bow;
	}
	private double precio_pico(String mat){
		return 3*getpreciomaterial(mat);
	}
	private double precio_hacha(String mat){
		return 3*getpreciomaterial(mat);
	}
	private double precio_espada(String mat){
		return 2*getpreciomaterial(mat);
	}
	private double precio_pala(String mat){
		return 1*getpreciomaterial(mat);
	}
	private double precio_hoz(String mat){
		return 2*getpreciomaterial(mat);
	}
	
	private double precio_armadura(String mat){
		return 8*getpreciomaterial(mat);
	}
	
	private double precio_pantalon(String mat){
		return 7*getpreciomaterial(mat);
	}	
	
	private double precio_botas(String mat){
		return 4*getpreciomaterial(mat);
	}
	private double precio_casco(String mat){
		return 5*getpreciomaterial(mat);
	}
	
	private double getpreciomaterial(String str){
		
		switch(str){
		case "leather":
			return Settings.material_leather;
		case "stone":
			return Settings.material_stone;
		case "wood":
			return Settings.material_wood;
		case "gold":
			return Settings.material_gold;
		case "iron":
			return Settings.material_iron;
		case "diamond":
			return Settings.material_diamond;
		case "chain":
			return Settings.material_chain;
	
		default:
			return Settings.material_diamond;
		}
	}

	
	

}
